#include "logos/gentoo_unicode.h"
#define COLOR "\e[1;34m"

#define TEMP_CACHE_FILE 1

#define CONFIG \
{ \
   /* name            function                 cached */\
    { "",             get_title,                    false }, \
    { "",             get_bar,                      false }, \
    { "OS: ",         get_os,                       true  }, \
    { "Host: ",       get_host,                     true  }, \
    { "Kernel: ",     get_kernel,                   true  }, \
    { "Uptime: ",     get_uptime,                   false }, \
    SPACER \
    { "Packages: ",   get_packages_portage,         false }, \
    { "Shell: ",      get_shell,                    true  }, \
    { "Terminal: ",   get_terminal,                 false }, \
    { "Theme: ",      get_theme,                    true  }, \
    SPACER \
    { "CPU: ",        get_cpu,                      true  }, \
    { "Memory: ",     get_memory,                   false }, \
    { "Disk Usage: ", get_disk_usage, false, "/home/me" }, \
    SPACER \
    { "",             get_colors1,                  false }, \
    { "",             get_colors2,                  false }, \
}

#define CPU_CONFIG                                                             \
  {                                                                            \
    REMOVE("(R)"), REMOVE("(TM)"), REMOVE("Dual-Core"), REMOVE("Quad-Core"),   \
        REMOVE("Six-Core"), REMOVE("Eight-Core"), REMOVE("Core"),              \
        REMOVE("CPU"),                                                         \
  }

#define GPU_CONFIG                                                             \
  { REMOVE("Corporation"), REMOVE("Integrated Graphics Controller"), }
